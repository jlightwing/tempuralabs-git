define flameOn = 1.
define flameOff = 0.

define gasOn = 1.
define gasOff = 0.

define ignitionOn = 1.
define ignitionOff = 0.

/* Defines the number of intervals per second */
define clockSpeed = $2$.

/* Defines the probability of flame failures, 100 always work, 0 never work*/
define blowoutProbability = 100.
define failedIgnitionProbability = 100.

/* Defines the seconds to be used in the safety and liveness condition checks */
define safetyConditionTime = $15$.
define livenessConditionTime = $20$.

/* Defines the maximum run length */
define length = 50.

/* run */ define main() = {
	exists IntervalNumber, Flame, Gas, Ignition, FlameExpected, FailedIgnitionEvents, ResetIgnition, IgnitionStartTime, BlowoutEvents, SafetyStop, LivenessTime, SafetyTime, CallForHeat : {
		halt(IntervalNumber = length or SafetyStop = 1) and
		initialise(IntervalNumber, Flame, Gas, Ignition, FlameExpected, FailedIgnitionEvents, ResetIgnition, IgnitionStartTime, BlowoutEvents, SafetyStop, LivenessTime, SafetyTime) and
		always {if more then {(next IntervalNumber) = IntervalNumber + 1}} and
		always {if more then {controller(Gas, Ignition, Flame, FlameExpected, ResetIgnition, IgnitionStartTime, IntervalNumber, SafetyStop, CallForHeat)}} and
		always {if more then {flameFailureCheck(Gas, Ignition, Flame, FlameExpected, FailedIgnitionEvents, ResetIgnition, IntervalNumber, IgnitionStartTime, BlowoutEvents, SafetyStop)}} and	
		always {if more then {nextFlame(Flame, Gas, Ignition)}} and
		always {if more then {format("Time=%f; Flame=%d; Gas=%d; Ignition=%d\nFailed Ignitions=%d; Reset Ignition=%d; Ignition Start Time=%f\nFlame Blowout Events=%d\n", 
						calculateTimeFromInterval(IntervalNumber), Flame, Gas, Ignition, FailedIgnitionEvents, ResetIgnition, IgnitionStartTime, BlowoutEvents)}} and
		always {
			if more then {verifyLivenessCondition(CallForHeat, Flame, calculateTimeFromInterval(IntervalNumber), LivenessTime)} and
			if more then {verifySafetyCondition(Flame, Gas, calculateTimeFromInterval(IntervalNumber), SafetyTime)}
		}
	
	}
}.

define calculateTimeFromInterval(Interval) = {
	itof(Interval) / clockSpeed
}.

define initialise(IntervalNumber, Flame, Gas, Ignition, FlameExpected, FailedIgnitionEvents, ResetIgnition, IgnitionStartTime, BlowoutEvents, SafetyStop, LivenessTime, SafetyTime) = {
	IntervalNumber = 0 and
	Flame = flameOff and
	Gas = gasOff and
	Ignition = ignitionOff and
	FlameExpected = 0 and 
	FailedIgnitionEvents = 0 and
	ResetIgnition = 0 and
	IgnitionStartTime = $0$ and
	BlowoutEvents = 0 and
	SafetyStop = 0 and 
	LivenessTime = $0$ and
	SafetyTime = $0$
}.

define controller(Gas, Ignition, Flame, FlameExpected, ResetIgnition, IgnitionStartTime, IntervalNumber, SafetyStop, CallForHeat) = {
	input CallForHeat and
	if (CallForHeat = 1 and ResetIgnition = 0) then {
		if (Flame = flameOff) then {
			Gas := gasOn and
			Ignition := ignitionOn and
			FlameExpected := 1 and
			
			/*Set start time, if start time is not set or zero, and copy start time to next state*/
			if (IgnitionStartTime = $0$) then {
				IgnitionStartTime := calculateTimeFromInterval(IntervalNumber)
			}
			else {
				IgnitionStartTime := IgnitionStartTime
			}
		}
		else {
			Gas := gasOn and
			Ignition := ignitionOff and
			FlameExpected := 1 and
			
			/*Reset start time*/
			IgnitionStartTime := $0$
		}
	}
	else {
		Gas := gasOff and
		Ignition := ignitionOff and
		FlameExpected := 0 and
		
		/*Reset start time*/
		IgnitionStartTime := $0$
	}
	
}.

define flameFailureCheck(Gas, Ignition, Flame, FlameExpected, FailedIgnitionEvents, ResetIgnition, Interval, IgnitionStartTime, BlowoutEvents, SafetyStop) = {
	if(FlameExpected = 1 and Flame = flameOff and Ignition = ignitionOn and ResetIgnition = 0) then {
		if ((calculateTimeFromInterval(Interval) - IgnitionStartTime) > $5$) then {
			FailedIgnitionEvents := FailedIgnitionEvents + 1 and
			BlowoutEvents := BlowoutEvents and
			ResetIgnition := 1 and
			format("Failed Ignitions\n")
		}
		else {
			FailedIgnitionEvents := FailedIgnitionEvents and
			BlowoutEvents := BlowoutEvents and
			ResetIgnition := 0
		}
	}
	else { if (FlameExpected = 1 and Flame = flameOff and Ignition = ignitionOff) then {
		ResetIgnition := 1 and
		FailedIgnitionEvents := FailedIgnitionEvents and
		BlowoutEvents := BlowoutEvents + 1 and
		format("Flame Blowout!\n")
	}
	else { 
		if (Flame = flameOn and Ignition = ignitionOff) then {
			FailedIgnitionEvents := 0 and
			BlowoutEvents := 0 and
			ResetIgnition := 0
		}
		else if (Flame = flameOn and Ignition = ignitionOn) then {
			FailedIgnitionEvents := 0 and
			BlowoutEvents := BlowoutEvents and
			ResetIgnition := 0
		}
		else {
			FailedIgnitionEvents := FailedIgnitionEvents and
			BlowoutEvents := BlowoutEvents and
			ResetIgnition := 0
		}
	}}
	and
	if (FailedIgnitionEvents = 3 or BlowoutEvents = 3) then {
		format("System has failed, please repair!\n") and
		SafetyStop := 1
	}
	else {
		SafetyStop := 0
	}
}.

define nextFlame(Flame, Gas, Ignition) = {
	if (Gas = gasOff) then {
		Flame := flameOff
	}
	else {
		if (Flame = flameOn) then {
			if (Random mod 100 >= blowoutProbability) then {Flame := flameOff} else {Flame := flameOn}
		}
		else {
			if (Ignition = ignitionOn) then {
				if (Random mod 100 >= failedIgnitionProbability) then {Flame := flameOff} else {Flame := flameOn}
			}
			else {
				Flame := flameOff
			}
		}
	}
}.

define verifySafetyCondition(Flame, Gas, Time, SafetyTime) = {
	if ((SafetyTime ~= $0$) and (Time - SafetyTime > safetyConditionTime)) then {
		format("--**Safety Fail**--\n")
	}
	else {
		format("--Safety Pass--\n")
	} and
	
	if ((Gas = gasOn) and (Flame = flameOff)) then {
		if (SafetyTime = $0$) then {
			SafetyTime := Time
		}
		else
		{
			SafetyTime := SafetyTime
		}
	} else {
		SafetyTime := $0$
	}
}.

define verifyLivenessCondition(CallForHeat, Flame, Time, LivenessTime) = {
	if ((LivenessTime ~= $0$) and (Time - LivenessTime > livenessConditionTime)) then {
		format("--**Liveness Fail**--\n")
	}
	else {
		format("--Liveness Pass--\n")
	} and
	
	if ((CallForHeat = 1) and (Flame = flameOff)) then {
		if (LivenessTime = $0$) then {
			LivenessTime := Time
		}
		else {
			LivenessTime := LivenessTime
		}
	} else {
		LivenessTime := $0$
	}
}.
