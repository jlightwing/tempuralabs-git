/* run */ define testa() =
exists A, B, I : {
	halt (I = 4) and (I = 0) and (A = 1) and (B = 1) and
	chopstar (
		A := (if I mod 2 = 1 then 1 else -1) and
		B := (if I mod 2 = 1 then 1 else 0) and
		skip and I := I + 1
	) and
	always (output (A) and output (B))
}.

/* run */ define testb() =
exists A, I : {
	halt (I = 9) and (I = 0) and (A = 0) and (A := 1) and
	{skip ; A gets (2-3*(A)) + prev(A) } and
	I gets I + 1 and
	always (output (A))
}.

/* run */ define testc() =
exists A, B, I, n : {
	input n and
	halt (I = n) and (I = 0) and (B = 0) and (if n > 0 then { B := 1}) and
	if n > 0 then {{skip ; 
	B gets (2-3*(B)) + prev(B) }} and
	I gets I + 1 and
	always {A = B*B }and
	always (output (A))
}.